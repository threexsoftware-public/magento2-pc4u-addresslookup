<?php

namespace ThreeXSoftware\Postcodes4uAddressLookup\Block\Js;
use Magento\Framework\View\Element\Template;

/**
 * Class Postcodes4uApi
 */
class Postcodes4uApi extends Template
{
    /**
     * @var array
     */
    private $fieldsMap;

    /**
     * @var PostcodeLookupConfigurationResolver
     */
    private $configurationResolver;

    /**
     * Postcodes4uApi constructor.
     *
     * @param Template\Context                    $context
     * @param PostcodeLookupConfigurationResolver $configurationResolver
     * @param array                               $fieldsMap
     * @param array                               $data
     */
    public function __construct(
        Template\Context $context,
        \ThreeXSoftware\Postcodes4uAddressLookup\System\PostcodeLookupConfigurationResolver $configurationResolver,
        $fieldsMap = [],
        array $data = []
    ) {
        parent::__construct($context, $data);

        $this->fieldsMap = $fieldsMap;
        $this->configurationResolver = $configurationResolver;
    }

    /**
     * @return array
     */
    public function getFieldsMap()
    {
        return $this->fieldsMap;
    }

    /**
     * @return string
     */
    public function getPc4uApiKey()
    {
        return $this->configurationResolver->getPc4uApiKey();
    }

    /**
     * @return string
     */
    public function getIsPc4uFrontEnabled()
    {
        return $this->configurationResolver->getIsPc4uFrontEnabled();
    }
    
    /**
     * @return string
     */
    public function getPc4uUsername()
    {
        return $this->configurationResolver->getPc4uUsername();
    }
}
