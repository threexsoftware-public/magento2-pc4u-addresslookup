# =============================================
# Postcodes4U Address Finder for Magento 2
# ---------------------------------------------
# Version 1.0 Dated 19th January 2021
# =============================================

## Downloading using composer
## --------------------------
- Request composer to fetch the Postcodes4u extension by entering:

    composer require threexsoftware-public/magento2-pc4u-addresslookup



## Manually Downloading
## --------------------
- Create the folder structure /app/code/ThreeXSoftware/Postcodes4uAddressLookup	
- Download & copy the archive contents to the new 
	/app/code/ThreeXSoftware/Postcodes4uAddressLookup folder 


## Installation Instructions
## -------------------------
Please be aware that the installation process below will take your Magento store 'offline' until they finished.
```
php -f bin/magento setup:upgrade
php -f bin/magento setup:di:compile
```
- The first stage allows Magento to recognize the module
- The second stage sets up the Postcodes4u and installs the configuration defaults. 


## Configuring the Postcodes4u Plugin
## ----------------------------------
Once this the installation is complete you will need to enable the Postcodes4u module via the admin interface.
The Postcodes4u settings can be found under the  
	Store->Configuration->3X Software->Postcodes4u settings. 
	From here you can add your Postcodes4u username and key which can be obtained from your account
	at the www.postcodes4u.co.uk website.
	

## MORE DETAILED INSTALLATION AND USAGE INSTRUCTIONS
## --------------------------------------------------

More detailed instructions on installing the Postcodes4u Magento2 extension can be found at:
https://www.postcodes4u.co.uk/support/Postcodes4u-Magento2-Setup.pdf
	
	

```
