<?php
/**
 * @package ThreeXSoftware\Postcodes4uAddressLookup
 */

namespace ThreeXSoftware\Postcodes4uAddressLookup\System;

use Magento\Framework\App\Config\ScopeConfigInterface;

/**
 * Class ConfigResolver
 */
class PostcodeLookupConfigurationResolver
{
    const PATH_PC4UF_IS_ENABLED = 'threexsoftware_postcodes4u/addresslookup/pc4u_front_enable';
    const PATH_PC4U_API_KEY = 'threexsoftware_postcodes4u/addresslookup/pc4u_api_key';
    const PC4u_USERNAME = 'threexsoftware_postcodes4u/addresslookup/pc4u_username';
    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * ConfigResolver constructor.
     *
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(ScopeConfigInterface $scopeConfig)
    {
        $this->scopeConfig = $scopeConfig;

    }

    /**
     * @return bool
     */
    public function getIsPc4uFrontEnabled(): bool
    {
        $feEnabled = $this->scopeConfig->getValue(self::PATH_PC4UF_IS_ENABLED);
        if($feEnabled === '1') {
            return true;
        } else {
            return false;
        }
        // return $this->scopeConfig->getValue(self::PATH_PC4UF_IS_ENABLED) === '1';
    }

    /**
     * @return string
     */
    public function getPc4uApiKey(): string
    {
        return $this->scopeConfig->getValue(self::PATH_PC4U_API_KEY);
    }
    
    /**
     * @return string
     */
    public function getPc4uUsername(): string
    {
        return $this->scopeConfig->getValue(self::PC4u_USERNAME);
    }
}
