
define(
    [
    'ko',
    'Magento_Ui/js/form/element/abstract',
    'Postcodes4uAddressLookup/model/apiLoadListener',
    'Postcodes4uAddressLookup/model/postcodes4u/initializer',
    'Postcodes4uAddressLookup/model/postcodes4u/strategy/default',
    'Postcodes4uAddressLookup/model/address/addressData',
    ], function (ko, Element, loadListener, Initializer, Strategy, addressData) {
        'use strict';




        return Element.extend(
            {
                onElementRender: function (el) {
                    if (!loadListener.isPostcodes4uApiLoaded()) {
                        loadListener.subscribe(
                            (isApiLoaded) => {
                                if (isApiLoaded) {
                                    this.initializer = new Initializer(el, Strategy, this.autocomplete_id);
                                }
                            }
                        );
                    } else {
                        this.initializer = new Initializer(el, Strategy, this.autocomplete_id);
                    }
                },
                initialize: function () {
                    this._super();
                    this.isShowDetails = addressData.getForm(this.autocomplete_id).isShowDetails;
                    this.value = ko.observable('');

                    addressData.getForm(this.autocomplete_id).country.subscribe(
                        () => {
                        this.value('');
                        }
                    );

                    return this;
                },
                toggleAddressData: function (data, evt) {
                    evt.preventDefault();
                    addressData.getForm(this.autocomplete_id).isShowDetails(true);
                },
                findAddress: function findAddress(data, evt)
                {
                    var addressId = this.autocomplete_id;
                    var strUrl = "",_this = jQuery;
                    var zipEnter = document.getElementById('lookup').value;
                    if(!zipEnter) {
                        _this('.show_address').html('Please enter postcode!');
                        _this('.show_address').css('color','red');
                    } else {
                        //Start loader
                        _this('body').loader('show');
                        //                strUrl =
                        //                    "https://services.3xsoftware.co.uk/Search/ByPostcode/json?username=" + encodeURI(window.Pc4uUserName)
                        //                    + "&key=" + encodeURI(window.Pc4uApiKey)
                        //                    + "&postcode=" + encodeURI(zipEnter);
                        strUrl =
                           "https://services.3xsoftware.co.uk/Search/ByPostcode/json?username=" + encodeURI(pc4uUserName)
                        + "&key=" + encodeURI(pc4uApiKey)
                        + "&postcode=" + encodeURI(zipEnter);
                        _this.ajax(
                            {
                                url: strUrl,
                                type: 'GET',
                                dataType: 'jsonp',
                                cors: true ,
                                contentType:'application/json',
                                secure: true,
                                showLoader: true,
                                headers: {
                                    'Access-Control-Allow-Origin': '*',
                                },
                                beforeSend: function (xhr) {
                                    xhr.setRequestHeader("Authorization", "Basic " + btoa(""));
                                },
                                success: function (data) {
                                    // Update Postcodes4u Dropdown - Set Default
                                    _this('.show_address_dropdown').empty();                        
                        
                                    if(data.Error && data.Error.Cause) {
                                        // Error Detected
                                         $('.show_address').html('Address Lookup Error: ' + data.Error.Description + ' : '+ data.Error.Cause);
                                         $('.show_address').css('color','red');
                                         $('body').loader('hide');
                                         _this('.show_address_dropdown').empty();
                        
                                    } else {
                                        if(data.Summaries && data.Summaries.length > 0) {
                                            // Create Postcodes4u Dropdown
                                            var pc4uM2AddDropdown =  _this(
                                                '<select>', {
                                                    'id': 'this.Id',
                                                    'name': 'pc4uM2AddDropdown',
                                                    'class':'pc4uM2AddDropdown',
                                                    'onchange': "return checkoutAddress('" + this.Id + "','" + addressId + "')"
                                                }
                                            );
                                            pc4uM2AddDropdown.append(
                                                _this(
                                                    '<option>', {
                                                        value: 0,
                                                        text: 'Select an address:'
                                                    }
                                                )
                                            );
                                
                                            _this.each(
                                                data.Summaries, function () {
                                    
                                                    //  Populate pc4u Dropdown
                                                    pc4uM2AddDropdown.append(
                                                        _this(
                                                            '<option>', {
                                                                value: this.Id,
                                                                text: (this.StreetAddress+', '+this.Place)
                                                            }
                                                        )
                                                    );                                    

                                                }
                                            );                              
                                        } else {                                
                                            // NO Data Returned
                                            $('.show_address').html('Address Lookup Error: NO ADDRESS DATA RETURNED');
                                            $('.show_address').css('color','red');
                                            $('body').loader('hide');
                     
                                        }
                            
                                        _this('body').loader('hide');
                            
                                        evt.preventDefault();
                                        addressData.getForm(addressId).isShowDetails(true);
                                    }   
                                },
                                error: function (data) {
                                    $('.show_address').css('color','red');
                                    _this(data).appendTo('.show_address');
                                    _this('body').loader('hide');
                                    console.error(data);
                                }
                            }
                        );
                    }
                }
            }
        );
    }
);
