'use strict';

define(
    ['ko', 'Magento_Ui/js/form/element/abstract', 'Postcodes4uAddressLookup/model/apiLoadListener', 'Postcodes4uAddressLookup/model/postcodes4u/initializer', 'Postcodes4uAddressLookup/model/postcodes4u/strategy/default', 'Postcodes4uAddressLookup/model/address/addressData',  'jquery', 'jquery/ui',], function (ko, Element, loadListener, Initializer, Strategy, addressData) {
        'use strict';
        var $ = jQuery.noConflict();
    
        return Element.extend(
            {
                onElementRender: function onElementRender(el)
                {
                    var _this2 = this;

                    if (!loadListener.isPostcodes4uApiLoaded()) {
                        loadListener.subscribe(
                            function (isApiLoaded) {
                                if (isApiLoaded) {
                                    _this2.initializer = new Initializer(el, Strategy, _this2.autocomplete_id);
                                }
                            }
                        );
                    } else {
                        this.initializer = new Initializer(el, Strategy, this.autocomplete_id);
                    }
                },
                initialize: function initialize()
                {
                    var _this3 = this;

                    this._super();
                    this.isShowDetails = addressData.getForm(this.autocomplete_id).isShowDetails;
                    this.value = ko.observable('');

                    addressData.getForm(this.autocomplete_id).country.subscribe(
                        function () {
                            _this3.value('');
                        }
                    );

                    return this;
                },
                toggleAddressData: function toggleAddressData(data, evt)
                {
                    evt.preventDefault();
                    addressData.getForm(this.autocomplete_id).isShowDetails(true);
                },
                findAddress: function findAddress(data, evt)
                {
                    var addressId = this.autocomplete_id;
                    var strUrl = "",
                    _this = jQuery;
                    var zipEnter = document.getElementById('lookup').value;
                    if (!zipEnter) {
                        _this('.show_address').html('Please enter postcode!');
                        _this('.show_address').css('color', 'red');
                    } else {
                        //Start loader
                        _this('body').loader('show');
                        //strUrl = "https://services.3xsoftware.co.uk/Search/ByPostcode/json?username=" + encodeURI(window.Pc4uUserName) + "&key=" + encodeURI(window.Pc4uApiKey) + "&postcode=" + encodeURI(zipEnter);
                        strUrl = "https://services.3xsoftware.co.uk/Search/ByPostcode/json?username=" + encodeURI(pc4uUserName) + "&key=" + encodeURI(pc4uApiKey) + "&postcode=" + encodeURI(zipEnter);
                        _this.ajax(
                            {
                                url: strUrl,
                                type: 'GET',
                                dataType: 'jsonp',
                                cors: true,
                                contentType: 'application/json',
                                secure: true,
                                showLoader: true,
                                headers: {
                                    'Access-Control-Allow-Origin': '*'
                                },
                                beforeSend: function beforeSend(xhr)
                                {
                                    xhr.setRequestHeader("Authorization", "Basic " + btoa(""));
                                },
                                success: function success(data)
                                {
                                     // Update Postcodes4u Dropdown - Set Default - KRF 20200514
                                    _this('.show_address_dropdown').empty();
                       
                                    _this('.show_address').empty();
                        
                                    if (data.Error && data.Error.Cause) {
                                         // Error Detected
                                         $('.show_address').html('Address Lookup Error: ' + data.Error.Description + ' : '+ data.Error.Cause);
                                         $('.show_address').css('color','red');
                                         $('body').loader('hide');                            
                            

                                    } else {
                                        if (data.Summaries && data.Summaries.length > 0) {
                                            var pc4uM2AddDropdown =  _this(
                                                '<select>', {
                                                    'id': 'this.Id',
                                                    'name': 'pc4uM2AddDropdown',
                                                    'class':'pc4uM2AddDropdown',
                                                    'onclick': "return checkoutAddress('" + addressId + "', this.value )"
                                                }
                                            );
                                            pc4uM2AddDropdown.append(
                                                _this(
                                                    '<option>', {
                                                        value: 0,
                                                        text: 'Select an address:'
                                                    }
                                                )
                                            );
                                
                                            //  Populate pc4u Dropdown
                                            _this.each(
                                                data.Summaries, function () {                               
                                                    // Add Option to Pc4u Dropdown
                                                    pc4uM2AddDropdown.append(
                                                        _this(
                                                            '<option>', {
                                                                value: this.Id,
                                                                text: (this.StreetAddress+', '+this.Place)
                                                            }
                                                        )
                                                    );
                                                }
                                            );
                                            // Show Pc4u Dropdown
                                            _this(pc4uM2AddDropdown).appendTo('.show_address_dropdown');
                                        } else {
                                            // NO Data Returned
                                            $('.show_address').html('Address Lookup Error: NO ADDRESS DATA RETURNED');
                                            $('.show_address').css('color','red');                                                                                             
                                        }
                            
                                        _this('body').loader('hide');
                                        evt.preventDefault();
                                        addressData.getForm(addressId).isShowDetails(true);
                                    }
                                },
                                error: function error(data)
                                {
                                    _this(data).appendTo('.show_address');
                                    _this('body').loader('hide');
                                    _this('.show_address_dropdown').empty();        
                                    console.error(data);
                                }
                            }
                        );
                    }
                }
            }
        );
    }
);
//# sourceMappingURL=addressLookup.js.map
