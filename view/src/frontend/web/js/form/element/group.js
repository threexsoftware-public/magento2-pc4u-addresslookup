
define(
    [
    'ko',
    'Magento_Ui/js/form/components/group',
    'Postcodes4uAddressLookup/model/address/addressData'
    ], function (ko, Element, addressData) {
        "use strict";

        return Element.extend(
            {
                initialize: function () {
                    this._super();
                    // KRF Test
                    this.visible(false);
                    // this.visible(true); - teST
            
                    this.template = 'ThreeXSoftware_Postcodes4uAddressLookup/form/element/group';

                    addressData.getForm(this.autocomplete_id).isShowDetails.subscribe(
                        (isEnterManually) => {
                        // KRF Test
                            this.visible(isEnterManually);
                        // this.visible(true); - test

                        }
                    );

                    return this;
                }
            }
        );
    }
);
